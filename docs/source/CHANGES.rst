CHANGES
=======

0.4.2 (2018/12/12)
------------------
- some Python 3 cleanup

0.4.1 (2017/10/05)
------------------
- further cleanup

0.4.0 (2017/08/26)
------------------
- removed outdated Dropbox implementation 

0.3.3 (2015/02/08)
------------------
- fix in constructor

0.3.2 (2015/01/27)
------------------
- registerTransformation no longer support duplicate 
  registrations
- minor fix in transformation code
  
0.3.0 (2014/11/17)
------------------
- code cleanup, removed obsolete modules
- drone.io support for continuous integration testing  

0.2.4 (2014/08/21)
------------------

- Transformer: fixed parameter handling

0.2.3 (2014/08/05)
------------------

- unicode fix

0.2.2 (2014/08/05)
------------------

- TransformerXML API is now unicode-aware


0.2.1 (2014/08/04)
------------------

- added TransformerXML API

0.2.0 (2014/03/16)
------------------

- enhanced resources_registry API 
- disabled Dropbox API due to Dropbox changes
- test fixes

0.1.0 (2013/07/11)
------------------

- initial release
