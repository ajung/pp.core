.. pp.core documentation master file, created by
   sphinx-quickstart on Wed Jul 10 19:13:03 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pp.core's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2

   README
   CHANGES

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

